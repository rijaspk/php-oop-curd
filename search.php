<?php
//paginating variables
include_once "config/core.php";

//database and object files
include_once "config/database.php";
include_once "objects/product.php";
include_once "objects/category.php";

//create database conncetion and objects
$databse = new Database();
$db = $databse->getConnection();

$product = new Product($db);
$category = new Category($db);

//get the search item
$searchkey = isset($_GET['key']) ? $_GET['key'] : die("Error : Search Value Not Found");

$page_title = "You have searched for \"{$searchkey}\"";

include_once "layouts/header.php";

// query products
$stmt = $product->search($searchkey, $from_record_num, $records_per_page);

// specify the page where paging is used
$page_url="search.php?s={$searchkey}&";

// count total rows - used for pagination
$total_rows=$product->countAll_BySearch($searchkey);

// read_template.php controls how the product list will be rendered
include_once "layouts/list_template.php";

// layout_footer.php holds our javascript and closing html tags
include_once "layouts/footer.php";
?>
