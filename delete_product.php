<?php
if ($_POST)
{
    include_once "config/database.php";
    include_once "objects/product.php";

    //database connection
    $database = new Database();
    $db = $database->getConnection();

    $product = new Product($db);

    $product->id = $_POST['object_id'];

    // delete the product
    if($product->deleteProduct()){
        echo "Object was deleted.";
    }

    // if unable to delete the product
    else{
        echo "Unable to delete object.";
    }

}
