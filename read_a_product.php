<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// set page headers
$id = isset($_GET['id'])? $_GET['id']    : die("ERROR: Id not Found");
include_once "config/database.php";
include_once "objects/product.php";
include_once "objects/category.php";

//set database connection
$database = new Database();
$db = $database->getConnection();

//set objects
$category = new Category($db);
$product = new Product($db);

//id property of product
$product->id = $id;

$product->readOne();

$page_title = "Product Details";
include_once "layouts/header.php";

// read products button
echo "<div class='right-button-margin'>";
echo "<a href='index.php' class='btn btn-primary pull-right showproductsbttn'>";
echo "<span class='glyphicon glyphicon-list'></span> Show Products";
echo "</a>";

echo "<table class='table table-hover table-responsive table-bordered'>";

echo "<tr>";
echo "<td>Name</td>";
echo "<td>{$product->name}</td>";
echo "</tr>";

echo "<tr>";
echo "<td>Price</td>";
echo "<td>&#36;{$product->price}</td>";
echo "</tr>";

echo "<tr>";
echo "<td>Description</td>";
echo "<td>{$product->description}</td>";
echo "</tr>";

echo "<tr>";
echo "<td>Category</td>";
echo "<td>";
// display category name
$category->id=$product->category_id;
$category->readName();
echo $category->name;
echo "</td>";
echo "</tr>";

echo "<tr>";
echo "<td>Image</td>";
echo "<td>";
echo $product->image ? "<img src='uploads/{$product->image}' style='width:300px;' />" : "No image found.";
echo "</td>";
echo "</tr>";

echo "</table>";
echo "</div>";

// set footer
include_once "layouts/footer.php";
?>