<?php
class Category{
    //database conncetion and tabele name
    private $conn;
    private $table_name = "categories";

    //object propeties
    public $id;
    public $name;

    /**
     * @return mixed
     */
    public function __construct($conn)
    {
        $this->conn=$conn;
    }

    public function read()
    {
        $query="SELECT id,name from ".$this->table_name." ORDER BY NAME";
            $stmt=$this->conn->prepare($query);
            $stmt->execute();
            return $stmt;

    }

    public function readName()
    {
        $query="SELECT name FROM ".$this->table_name." where id=? limit 0,1";
        $stmt=$this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row=$stmt->fetch(PDO::FETCH_ASSOC);

        $this->name=$row['name'];
    }
}
?>