<?php

class Database
{
    Private $host="localhost";
    Private $db_name="php_crud";
    Private $username="root";
    Private $password="";
    Private $conn;

    /**
     * @return mixed
     */
    public function getConnection()
    {
        $this->conn=null;
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
}