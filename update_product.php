<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// retrieve one product will be here
$id = isset($_GET['id']) ? ($_GET['id']) : die('ERROR : Id not found');
include 'objects/category.php';
include "objects/product.php";
include "config/database.php";
//database connection
$database = new Database();
$db = $database->getConnection();

//objects
$categroy=new Category($db);
$product = new Product($db);

//product id
$product->id = $id;

$product->readOne();


// set page header
$page_title="Update Product";
include 'layouts/header.php';

// contents will be here
echo "<div class='right-button-margin'>";
echo "<a href='index.php' class='btn btn-default pull-right'>Read Products</a>";
echo "</div>";
?>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?id={$id}");?>" method="post" enctype="multipart/form-data">
    <table class='table table-hover table-responsive table-bordered'>

        <tr>
            <td>Name</td>
            <td><input type='text' name='name' value='<?php echo $product->name; ?>' class='form-control' /></td>
        </tr>

        <tr>
            <td>Price</td>
            <td><input type='text' name='price' value='<?php echo $product->price; ?>' class='form-control' /></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><textarea name='description' class='form-control'><?php echo $product->description; ?></textarea></td>
</tr>

<tr>
    <td>Category</td>
    <td>
        <!-- categories select drop-down will be here -->
        <?php
        $stmt = $categroy->read();
        // put them in a select drop-down
        echo "<select class='form-control' name='category_id'>";

        echo "<option>Please select...</option>";
        while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)){
            $category_id=$row_category['id'];
            $category_name = $row_category['name'];

            // current category of the product must be selected
            if($product->category_id==$category_id){
                echo "<option value='$category_id' selected>";
            }else{
                echo "<option value='$category_id'>";
            }

            echo "$category_name</option>";
        }
        echo "</select>";
        ?>

    </td>
</tr>

        <tr>
            <td>Product Image</td>
            <td>
                <?php echo $product->image ? "<img src='uploads/{$product->image}' style='width:300px;' />" : "No image found."; ?>

            </td>
        </tr>



    <tr>
        <td>Update Image</td>
        <td> <input type="file" name="image" id="file-input">
        <div id="thumb-output"></div></td>
    </tr>

<tr>
    <td></td>
    <td>
        <button type="submit" class="btn btn-primary">Update</button>
    </td>
</tr>

</table>
</form>
<script>
    $(document).ready(function(){
        $('#file-input').on('change', function(){ //on file input change
            if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
            {
                $('#thumb-output').html(''); //clear html of output element
                var data = $(this)[0].files; //this file data

                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                            return function(e) {
                                var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                                $('#thumb-output').append(img); //append image to output element
                            };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });

            }else{
                alert("Your browser doesn't support File API!"); //if File API is absent
            }
        });
    });

</script>

<?php
// set page footer
include 'layouts/footer.php';
?>
<?php
if($_POST)
{

    //set product values
    $product->name = $_POST['name'];
    $product->price = $_POST['price'];
    $product->description = $_POST['description'];
    $product->category_id = $_POST['category_id'];
//    $image=!empty($_FILES["image"]["name"])
//        ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES["image"]["name"]) : "";
//
//    $product->image = $image;

    // update the product
    if($product->update()){

        echo "<div class='alert alert-success alert-dismissable'>";
        echo "Product was updated.";
        echo "</div>";
//        echo $product->uploadPhoto();

    }

    // if unable to update the product, tell the user
    else{
        echo "<div class='alert alert-danger alert-dismissable'>";
        echo "Unable to update product.";
        echo "</div>";
    }
}
?>

















